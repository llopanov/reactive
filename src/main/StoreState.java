import interfaces.Action;
import interfaces.SomeState;

class StoreState {
    private SomeState currentState;

    StoreState(SomeState state){
        this.currentState = state;
    }
    public SomeState getState() { return currentState; }

    public void dispatch(Action<SomeState> action) {
        currentState = action.apply(currentState);
    }
}