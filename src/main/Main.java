import Implementations.IncrementActionImpl;
import Implementations.StateIntImpl;
import interfaces.Action;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main {
    public static void main(String[] args) {
        StateIntImpl state = new StateIntImpl(50);

        StoreState store = new StoreState(state);
        Action action = new IncrementActionImpl();

        log.info("state - {}", store.getState().getValue());
        store.dispatch(action);

        log.info("state - {}", store.getState().getValue());
    }
}
