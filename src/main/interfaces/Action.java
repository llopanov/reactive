package interfaces;

public interface Action<SomeState> {
    SomeState apply(SomeState currentState);
}
