package Implementations;

import interfaces.SomeState;

public class StateIntImpl implements SomeState<Integer> {
    private Integer value;

    public StateIntImpl(Integer value) {
        this.value = value;
    }

    public StateIntImpl(SomeState<Integer> state) {
        value = state.getValue();
    }

    public Integer getValue(){
        return this.value;
    }

    @Override
    public void setValue(Integer value) {
        this.value = value;
    }

//    public interfaces.SomeState clone() throws CloneNotSupportedException {
//        return (interfaces.SomeState) super.clone();
//    }
}
