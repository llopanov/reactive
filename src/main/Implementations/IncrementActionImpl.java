package Implementations;

import interfaces.Action;
import interfaces.SomeState;

public class IncrementActionImpl implements Action<SomeState<Integer>> {

    public SomeState<Integer> apply(SomeState<Integer> state) {

        SomeState<Integer> newState = new StateIntImpl(state);
        newState.setValue(state.getValue() + 1);
        return newState;
    }
}
